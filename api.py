from math import sqrt

from fastapi import APIRouter



router = APIRouter()


@router.get("/")
async def read(a: int, b: int, c: int):
    d = b*b - 4 * a * c
    if d < 0:
        answer = "решений нет"
    elif d == 0:
        x = -b / (2 * a)
        answer = f"уравнение имеет одно решение {x}"
    else:
        x1 = (-b + sqrt(d)) / (2 * a)
        x2 = (-b - sqrt(d)) / (2 * a)
        answer = f"уравнение имеет два решения: {x1} и {x2}"
    return answer

